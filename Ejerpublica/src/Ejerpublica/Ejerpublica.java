import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.PrintWriter;
import java.security.KeyFactory;
import java.security.KeyPair;

import java.security.KeyPairGenerator; 
import java.security.spec.RSAPublicKeySpec; 
import javax.crypto.Cipher; 

public class Ejerpublica { 
public static void main(String[] args) throws FileNotFoundException 
{ 
    try { 

// En esta linea se generan las claves publicas y privadas para el algoritmo RSA
KeyPairGenerator keygen = KeyPairGenerator.getInstance("RSA"); 


// Con esta linea se generan las claves privdas y publicas mediante el objeto KeyPairGenerator
KeyPair keypair = keygen.generateKeyPair(); 

// Con esta linea se crea el objeto para descifrar la clave. EL objeto Cipher permite tanto descifrar como cifrar.
Cipher desCipher = Cipher.getInstance("RSA"); 

desCipher.init(Cipher.ENCRYPT_MODE, keypair.getPrivate()); 

// Con las 2 siguientes lineas se "abre el fichero" que se cifrara
File inf = new File("fichero_prueba.txt"); 
FileInputStream is = new FileInputStream(inf); 

// Con este try catch se abre el fichero cifrado y se van lee todo el fichero almacenando los datos en un array después de descifrarlos
try (FileOutputStream os = new FileOutputStream("fichero_cifrado_con_RSA")) {
           
            byte[] buffer = new byte[64];
            int bytes_leidos = is.read(buffer);
            while (bytes_leidos != -1) {
                byte[] cbuf = desCipher.doFinal(buffer, 0, bytes_leidos);
                os.write(cbuf);
                bytes_leidos = is.read(buffer);
            } }

      




KeyFactory keyfac = KeyFactory.getInstance("RSA"); 
System.out.println("Generando keyspec");
RSAPublicKeySpec publicKeySpec = keyfac.getKeySpec(keypair.getPublic(), RSAPublicKeySpec.class);

// En estas lineas se guardará la clave publica. Primero se abrirá el fichero y luego se guardará la información en el
FileOutputStream cos = new FileOutputStream("clave_publica"); 
PrintWriter pw = new PrintWriter(cos); 
pw.println(publicKeySpec.getModulus()); 
      pw.println(publicKeySpec.getPublicExponent());
pw.close(); 
} 
catch (Exception e) { e.printStackTrace(); 
} 

} 
} 






