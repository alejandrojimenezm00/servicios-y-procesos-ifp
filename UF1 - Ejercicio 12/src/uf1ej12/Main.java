package uf1ej12;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class Main {

	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {

		int eleccion = 1;
		
		// Las variables aqui declaradas sirven para lo siguiente:
		/*
		 * Guardar el nombre del fichero
		 * Guardar la ruta, he optado por el uso de rutas absolutas antes que relativas, esto lo he hecho porque con relativas no logro que lo haga cifrar
		 * La variable file esta declarada para guardar el fichero
		 * 
		 * Las variables byte para almacenar la información de los datos para cifrar
		 * 
		 */
	    String nombreArchivo = "";
	    String rutaPrincipal = "C:\\cifrado\\";
	    Path rutaFinal;
	    File fichero;
	    Cipher desCipher;
	    byte[] lectura;
	    byte[] mensajeCifrado;
	    byte[] mensajeDescifrado;
		
		
		// Con las siguientes lineas genero las claves DES
	    KeyGenerator keygen = null;
	    try {
	        keygen = KeyGenerator.getInstance("DES");
	    } catch (NoSuchAlgorithmException e) {
	        e.printStackTrace();
	    }
	    SecretKey key = keygen.generateKey();
		
		
	    // Inicio del menu
	    
		while (eleccion > 0 && eleccion < 3) {
			
			System.out.println("|********************************|\n" 
							  +"|Opcion 1: cifrar               *|\n"
					          +"|Opcion 2: Descifrar            *|\n"
							  +"|Opcion 3: salir                *|\n"
					          +"|Eleccion:                      *|\n");
			eleccion = sc.nextInt();
			sc.nextLine();
			
			switch (eleccion) {
			
			case 1:
                // Pregunto por el nombre de fichero para cifrar
                System.out.print("Introduce el nombre del fichero para cifrar: ");
                nombreArchivo = sc.nextLine();
                
                // Aplico el valor de lo que el usuario a introducido a las variables del fichero
                rutaFinal = Paths.get("C:\\cifrado\\" + nombreArchivo);
                fichero = new File("C:\\cifrado\\" + nombreArchivo);
                
                System.out.println(fichero.toString());

                // Se comprueba si el fichero existe o no
                
                if (!fichero.exists()) {
                    System.out.println("El fichero no existe, volviendo al menu principal");
                    continue;
                } else if (!fichero.canRead()) {
                    System.out.println("No se puede leer el fichero, volviendo al menu principal");
                } else {
                    try {
                        // Leer el archivo y guardar sus datos en una cadena
                        lectura = Files.readAllBytes(rutaFinal); //Leer el contenido del fichero
                        System.out.println("El contenido del fichero para cifrar es: " + new String(lectura));

                        // Usar la llave para cifrar
                        desCipher = Cipher.getInstance("DES");
                        desCipher.init(Cipher.ENCRYPT_MODE, key);

                        // Cifrar el contenido del fichero
                        mensajeCifrado = desCipher.doFinal(lectura);

                        // Guardar el contenido del fichero cifrado en un nuevo fichero
                        System.out.print("Introduce el nombre del fichero para guardar el cifrado: ");
                        nombreArchivo = sc.nextLine();
                        fichero = new File(rutaPrincipal + nombreArchivo);
                        FileOutputStream fos = new FileOutputStream(fichero);
                        fos.write(mensajeCifrado);
                        fos.close();
                        System.out.println("Fichero cifrado creado: " + rutaPrincipal + nombreArchivo);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case 2:
                // Obtener nombre de fichero para cifrar
                System.out.print("Introduce el nombre del fichero para descifrar: ");
                nombreArchivo = sc.nextLine();
                rutaFinal = Paths.get("C:\\cifrado\\" + nombreArchivo);
                fichero = new File("C:\\cifrado\\" + nombreArchivo);

                // Se comprueba si el fichero existe o no
                
                if (!fichero.exists()) {
                    System.out.println("El fichero no existe, volviendo al menu principal");
                    continue;
                } else if (!fichero.canRead()) {
                    System.out.println("No se puede leer el fichero, volviendo al menu principal");
                } else {
                    try {
                        // Leer el archivo y guardar sus datos en una cadena
                        lectura = Files.readAllBytes(rutaFinal); //Leer el contenido del fichero
                        System.out.println("El contenido del fichero cifrado es: " + new String(lectura));

                        // Usar la llave para cifrar
                        desCipher = Cipher.getInstance("DES");
                        desCipher.init(Cipher.DECRYPT_MODE, key);

                        //Cifrar el contenido del fichero
                        mensajeDescifrado = desCipher.doFinal(lectura);
                        System.out.println("El mensaje original era: " + new String(mensajeDescifrado));

                        // Guardar el contenido del fichero cifrado en un nuevo fichero
                        fichero = new File(rutaPrincipal + "DESCIFRADO_" + nombreArchivo.substring(nombreArchivo.indexOf("_")));
                        FileOutputStream fos = new FileOutputStream(fichero);
                        fos.write(mensajeDescifrado);
                        fos.close();
                        System.out.println("Fichero cifrado creado: " + rutaPrincipal + "DESCIFRADO_" + nombreArchivo.substring(nombreArchivo.indexOf("_")));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case 3:
                System.out.println("Fin del programa");
                break;
            default:
                System.out.println("Introduzca un valor válido");
                break;
        }
			
			}
			
		}
		
	}
