package uf1ej11;

import java.util.Scanner;

public class Main {

	// variable estatica del scanner para que pueda ser usada en toda la clase
	static Scanner sc = new Scanner(System.in);
	
	public static String cifrarTexto (String texto) {
		
		// Con esta linea separo todos los caracteres de la palabra que nos pasa el usuario por referencia.
		// COn esto luego puedo operar caracter a caracter
		String[] cadena = texto.split("");
		
		// Con este bucle recorro el array con la palabra
		for (int i = 0; i < cadena.length; i++) {
			
			// El algoritmo que he planteado es el siguiente: convierto el caracter en decimal, luego combierto el código de color 67080b en decimal (ya que todos los colores estan en hexadecimal) y con eso saco un número.
			cadena[i] = String.valueOf(Integer.parseInt(String.format("%04x", (int) cadena[i].charAt(0)), 16) * Integer.parseInt ("67080b", 16));
			
		}
		
		// Devuelvo la cadena convertida a un solo string
		return String.join("", cadena);
		
	}
	
	public static String descifrarTexto (String texto, String textoGuia) {
		
		// El cifrado que he pensado siempre combierte un char en un conjunto de números de 9 digitos
		
		// al igual que cuando cifro, con esta linea guardo todo la cadena cifrada en un array
		String[] cadena = texto.split("");
		
		// Uso esta linea como linea de control
		String cadenaGuia = "";
		
		for (int i = 0; i < cadena.length; i += 9) {
			
			// Declaro un String de control que luego concatenare a la cadenaGuia
			String control = "";
			
			for (int x = 0; x < 9; x++) {
				
				// Con esta lienea cojo los 9 siguientes puestos del array para luego operar con ellos
				control += cadena[i+x];
				
			}
			
			// A cadenaGuia le aplico la operación contraria al algoritmo para cifrar, es decir, a las 9 posiciones que he separado las divido por el codigo de color 67080b
			cadenaGuia += Character.toString(Integer.parseInt(control) / Integer.parseInt ("67080b", 16));
						
		}
		
		
		return cadenaGuia;
		
	}
	
	public static void main(String[] args) {
		
		// Estas 2 variables sirven para almacenar la palabra que queremos cifrar y luego descifrar.
		String textoUsuario = "";
		String textoTrabajar = "";
		
		// Con estas 2 lineas almacenamos en la variable textoUsuario la palabra que queremos cifrar
		System.out.println("Palabra a cifrar: ");
		textoUsuario = sc.nextLine();
		
		// Con esta linea almaceno la palabra cifrada en la variable textoTrabajar.
		textoTrabajar = cifrarTexto(textoUsuario);
		
		// Imprimo el codigo cifrado
		System.out.println("Este es tu texto cifrado: " + textoTrabajar);
		
		// Hago una llamada al metodo para descrifrar, e imprimo lo que devuelve.
		System.out.println("Este es tu texto descifrado: " + descifrarTexto(textoTrabajar, textoUsuario));
		
		

	}

}
